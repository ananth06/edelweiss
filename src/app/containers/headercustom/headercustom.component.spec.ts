import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadercustomComponent } from './headercustom.component';

describe('HeadercustomComponent', () => {
  let component: HeadercustomComponent;
  let fixture: ComponentFixture<HeadercustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadercustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadercustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
